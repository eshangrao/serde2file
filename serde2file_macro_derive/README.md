# [serde2file](https://crates.io/crates/serde2file)'s derive macro

## usage / 用法

```ignore
#[derive(Serialize, Deserialize, Serde2File)]
#[serde2file(
    encrypt = "TestEncryptTool::encrypt",
    decrypt = "TestEncryptTool::decrypt",
    crypt_arg_type = "&'static str",
    dump_file_name = "test_data.json"
)]
```

# Attributes / 属性 :

* #[serde2file(arg1=value1,...)]
  * encrypt Data encryption method
  * decrypt Data decryption method
  * The above encryption and decryption methods must be set at the same time, otherwise encryption and decryption will not be performed.
  * example : #[serde2file(encrypt="TestEncryptTool::encrypt",decrypt="TestEncryptTool::decrypt")]
  * dump_file_name
    * Custom dump file name
    * If not set ,the default dump file name is the full name of the current Struct.
    * For example, the default dump file name corresponding to serde2file::test::TestData is serde2file-test-TestData.json
    * example : #[serde2file(dump_file_name = "test_data.json")]
  * crypt_arg_type
    * Define additional encryption and decryption parameter types
    * Used to pass custom parameters to encryption or decryption functions
    * example : #[serde2file(crypt_arg_type = "&'static str")]
  * Dynamically determine the file name and save path
    * file_name_getter : File name personalization acquisition function
    * file_name_getter_arg_type : File name acquisition function parameter type, used to dynamically obtain file name according to parameters

* #[serde2file(参数1=值1,...)]
  * encrypt 数据加密方法
  * decrypt 数据解密方法
  * 以上加密和解密方法必须同时设置，否则不进行加解密。
  * 例如：#[file_encrypt(encrypt="TestEncryptTool::encrypt",decrypt="TestEncryptTool::decrypt")]
  * dump_file_name
    * 设置自定义的转储文件名称
    * 自定义转储文件名称
    * 默认为当前Struct的完整名称，
    *  如serde2file::test::TestData对应的缺省转储文件名称为serde2file-test-TestData.json
    * 例如：#[serde2file(dump_file_name = "test_data.json")]
  * crypt_arg_type
    * 定义额外的加解密参数类型
    * 用于向加密或解密函数传递自定义参数使用
    * 例如：#[serde2file(crypt_arg_type = "&'static str")]
  * 动态确定文件名称和保存路径
    * file_name_getter 文件名称获取函数
    * file_name_getter_arg_type 文件名称获取函数传参类型，用于根据参数动态获取文件名称
    * 例如：#[serde2file(file_name_getter = "some_get_file_name_function",file_name_getter_arg_type = "String")]

# Examples/例子

```rust
use serde2file_macro_derive::Serde2File;

#[derive(Debug, Serialize, Deserialize, Default, PartialEq, Eq, Serde2File)]
#[serde2file(
    encrypt = "TestEncryptTool::encrypt",
    decrypt = "TestEncryptTool::decrypt",
    crypt_arg_type = "&'static str",
    file_name_getter_arg_type = "(&str,&str)",
    file_name_getter = "(&str,&str)",
    dump_file_name = "test_data.json",   
    file_name_getter = "some_get_file_name_function",
    file_name_getter_arg_type = "String"
)]
struct TestData {
    id: String,
    name: String,
}
```