# Serde2File

---

serialize some data to a file or deserialize a data from a file by serde_json

1. Support:

* Encrypted storage is supported when storing files
* Support user-defined encryption method and parameter transfer
* Provide default aes256gsm encryption implementation
* Flexible file storage location

2. derive macro [Serde2File](https://crates.io/crates/serde2file_macro_derive) Attributes（Optional）:

## usage

```rust
#[derive(Serialize, Deserialize, Serde2File)]
#[serde2file(
    encrypt = "TestEncryptTool::encrypt",
    decrypt = "TestEncryptTool::decrypt",
    crypt_arg_type = "&'static str",
    file_name_getter_arg_type = "(&str,&str)",
    file_name_getter = "(&str,&str)",
    dump_file_name = "test_data.json",   
    file_name_getter = "some_get_file_name_function",
    file_name_getter_arg_type = "String"
)]
```

## Attributes :

* #[serde2file(arg1=value1,...)]
  * encrypt Data encryption method
  * decrypt Data decryption method
  * The above encryption and decryption methods must be set at the same time, otherwise encryption and decryption will not be performed.
  * example : #[serde2file(encrypt="TestEncryptTool::encrypt",decrypt="TestEncryptTool::decrypt")]
  * dump_file_name
    * Custom dump file name
    * If not set ,the default dump file name is the full name of the current Struct.
    * For example, the default dump file name corresponding to serde2file::test::TestData is serde2file-test-TestData.json
    * example : #[serde2file(dump_file_name = "test_data.json")]
  * crypt_arg_type
    * Define additional encryption and decryption parameter types
    * Used to pass custom parameters to encryption or decryption functions
    * example : #[serde2file(crypt_arg_type = "&'static str")]
  * Dynamically determine the file name and save path
    * file_name_getter : File name personalization acquisition function
    * file_name_getter_arg_type : File name acquisition function parameter type, used to dynamically obtain file name according to parameters

3. Features

* default : core + encrypt_tool
* core : without encrypt_tool
* encrypt_tool : a Aes256Gcm crypt tool


---

将struct序列化化存储为文件，或者从文件反序列化为struct



1. 支持：

* 文件存储时支持加密存储
* 支持自定义加密方法和参数传递
* 提供默认的aes256gsm加密实现
* 灵活的文件存储路径和名称设置

2. 派生宏[Serde2File](https://crates.io/crates/serde2file_macro_derive)属性(可选)

## 用法

```rust
#[derive(Serialize, Deserialize, Serde2File)]
#[serde2file(
    encrypt = "TestEncryptTool::encrypt",
    decrypt = "TestEncryptTool::decrypt",
    crypt_arg_type = "&'static str",
    file_name_getter_arg_type = "(&str,&str)",
    file_name_getter = "(&str,&str)",
    dump_file_name = "test_data.json",   
    file_name_getter = "some_get_file_name_function",
    file_name_getter_arg_type = "String"
)]
```

## 属性 :

* #[serde2file(参数1=值1,...)]
  * encrypt 数据加密方法
  * decrypt 数据解密方法
  * 以上加密和解密方法必须同时设置，否则不进行加解密。
  * 例如：#[file_encrypt(encrypt="TestEncryptTool::encrypt",decrypt="TestEncryptTool::decrypt")]
  * dump_file_name
    * 设置自定义的转储文件名称
    * 自定义转储文件名称
    * 默认为当前Struct的完整名称，
    *  如serde2file::test::TestData对应的缺省转储文件名称为serde2file-test-TestData.json
    * 例如：#[serde2file(dump_file_name = "test_data.json")]
  * crypt_arg_type
    * 定义额外的加解密参数类型
    * 用于向加密或解密函数传递自定义参数使用
    * 例如：#[serde2file(crypt_arg_type = "&'static str")]
  * 动态确定文件名称和保存路径
    * file_name_getter 文件名称获取函数
    * file_name_getter_arg_type 文件名称获取函数传参类型，用于根据参数动态获取文件名称
    * 例如：#[serde2file(file_name_getter = "some_get_file_name_function",file_name_getter_arg_type = "String")]

3. 特性

* 默认 : core + encrypt_tool
* core 无加密工具
* encrypt_tool : Aes256Gcm 加密工具

1. Example1 / 示例1

(1) Simplest use / 最简单用法

Without encryption, dump the struct directly to a json file. The dump file name is the default name

无需加密，直接将struct转储到json文件,转储文件名称为默认名称

```rust
use serde::{Deserialize, Serialize};
use serde2file::prelude::*;

#[derive(Debug, Serialize, Deserialize, Default, PartialEq, Eq, Serde2File)]
struct TestData {
    id: String,
    name: String,
}
#cfg(test)
mod test {
    use super::*;
    /// Test serialization and encrypted storage to file
    /// 测试序列化并加密存储到文件
    #[test]
    fn test_serialize_to_file() {
        let data = TestData {
            id: "01".into(),
            name: "hy".into(),
        };
        assert_eq!("/tmp/[package-name]-Test-Data.json",
            data.dump2file("/tmp").unwrap());
    }
    /// Test deserialization from encrypted file
    /// 测试从加密文件反序列化
    #[test]
    fn test_deserialize_from_file() {
        let data = TestData {
            id: "01".into(),
            name: "hy".into(),
        };
        let s_data = TestData::load_from_file("/tmp").unwrap();
        assert_eq!(data, s_data)
    }
}
```

(2) Encrypted dump to custom file / 加密转储为自定义文件

Encrypt the dump file. The default aes256gsm encryption is used for encryption.   
No additional encryption parameters are required.   
Set the dump file name to test_ data.json.

加密转储文件，加密时使用默认的aes256gsm加密实现，无需额外加密参数,设置转储文件名称为test_data.json.

```rust
use serde::{Deserialize, Serialize};
use serde2file::encrypt::Aes256GcmEncryptUtil;
use serde2file::prelude::*;

#[allow(dead_code)]
#[derive(Debug, Serialize, Deserialize, Default, PartialEq, Eq, Serde2File)]
#[serde2file(
    dump_file_name = "test_data.json",
    encrypt = "TestEncryptTool::encrypt",
    decrypt = "TestEncryptTool::decrypt"
)]
struct TestData {
    id: String,
    name: String,
}
/// a encryption and decryption tools
#[allow(dead_code)]
struct TestEncryptTool;
impl Aes256GcmEncryptUtil for TestEncryptTool{
    type CryptArgType = ();
    fn get_aes_key_nonce(extra:Option<Self::CryptArgType>) -> (String, String) {
        (
            format!("*)_#@{}!$=_^20230208)leb*$xz",extra.unwrap_or("140600")),
            "abc$hy%95599".into(),
        )
    }
}

/// Test serialization and encrypted storage to file
/// 测试序列化并加密存储到文件
#[test]
fn test_serde_file() {
    let data = TestData {
        id: "01".into(),
        name: "hy".into(),
    };
    assert_eq!("/tmp/test_data.json",data.dump2file("/tmp").unwrap());
    let s_data = TestData::load_from_file("/tmp").unwrap();
    assert_eq!(data, s_data)
}

```


(3) 自定义加密参数加密转储文件

The default aes256gsm encryption is used for encryption,
and additional encryption and decryption parameters are required.
Set the dump file name to test_ data.json.

加密时使用默认的aes256gsm加密实现，并需要额外加解密参数,设置转储文件名称为test_data.json


```rust
use serde::{Deserialize, Serialize};
use super::encrypt::Aes256GcmEncryptUtil;
use crate::prelude::*;

#[allow(dead_code)]
#[derive(Debug, Serialize, Deserialize, Default, PartialEq, Eq, Serde2File)]
#[serde2file(
    dump_file_name = "test_data2.json",
    encrypt = "TestEncryptTool::encrypt",
    decrypt = "TestEncryptTool::decrypt",
    crypt_arg_type = "&'static str"
)]
struct TestData2 {
    id: String,
    name: String,
}

/// a encryption and decryption tools
#[allow(dead_code)]
struct TestEncryptTool;
impl Aes256GcmEncryptUtil for TestEncryptTool{
    type CryptArgType = &'static str;
    fn get_aes_key_nonce(extra:Option<Self::CryptArgType>) -> (String, String) {
        (
            format!("*)_#@{}!$=_^20230208)leb*$xz",extra.unwrap_or("140600")),
            "abc$hy%95599".into(),
        )
    }
}

/// Test serialization and encrypted storage to file with extra param
/// 测试序列化并加密存储到文件
#[test]
fn test_serde_file() {
    let data = TestData2 {
        id: "01".into(),
        name: "hy".into(),
    };
    let file_name_arg = String::from("14H701");
    assert_eq!("/tmp/test_data2.json",data.dump2file_with_encrypt_arg("/tmp",Some("14H701"),Some(file_name_arg)).unwrap());
    let s_data = TestData2::load_from_file_with_decrypt_arg("/tmp",Some("14H701"),Some(file_name_arg)).unwrap();
    assert_eq!(data, s_data)
}

```

(4) 自定义加密参数加密转储文件,并动态确定文件存储名称

The default aes256gsm encryption is used for encryption,
and additional encryption and decryption parameters are required.
get the dump file name by get_file_name.

加密时使用默认的aes256gsm加密实现，并需要额外加解密参数,通过get_file_name函数获取文件名称


```rust
use serde::{Deserialize, Serialize};
use super::encrypt::Aes256GcmEncryptUtil;
use crate::prelude::*;

fn get_file_name(sys_root:&str,extra:Option<String>) -> DumpResult<String>{
    Ok(format!("{sys_root}/{}",extra.unwrap()))
}

#[allow(dead_code)]
#[derive(Debug, Serialize, Deserialize, Default, PartialEq, Eq, Serde2File)]
#[serde2file(
    encrypt = "TestEncryptTool::encrypt",
    decrypt = "TestEncryptTool::decrypt",
    crypt_arg_type = "&'static str"，
    file_name_getter = "get_file_name",
    file_name_getter_arg_type = "String"
)]
struct TestData2 {
    id: String,
    name: String,
}

/// a encryption and decryption tools
#[allow(dead_code)]
struct TestEncryptTool;
impl Aes256GcmEncryptUtil for TestEncryptTool{
    type CryptArgType = &'static str;
    fn get_aes_key_nonce(extra:Option<Self::CryptArgType>) -> (String, String) {
        (
            format!("*)_#@{}!$=_^20230208)leb*$xz",extra.unwrap_or("140600")),
            "abc$hy%95599".into(),
        )
    }
}

/// Test serialization and encrypted storage to file with extra param
/// 测试序列化并加密存储到文件
#[test]
fn test_serde_file_extra2() {
    let data = TestData2 {
        id: "01".into(),
        name: "hy".into(),
    };

    let file_name_arg = String::from("14H701");
    assert_eq!(
        format!("/tmp/{fiel_name_arg}"),
        data.dump2file_with_encrypt_arg_and_path(
            "/tmp",
            Some("14H701"),
            Some(file_name_arg)
        ).unwrap());
    let s_data = TestData2::load_from_file_with_decrypt_arg_and_path("/tmp",Some("14H701"),Some(file_name_arg)).unwrap();
    assert_eq!(data, s_data)
}

```